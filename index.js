/*

1. What built-in JS keyword is used to import packages?
    Answer: require()
2. What Node.js module/package contains a method for server creation?
    Answer: 'http'
3. What is the method of the http object responsible for creating a server using Node.js?
    Answer: createServer()
4. Between server and client, which creates a request?
    Answer: Client creates a request 
5. Between server and client, which creates a response?
    Answer: Server creates a response
6. What is a runtime environment used to create stand-alone backend applications written in Javascript?
    Answer: Node.js
7.What is the largest registry for Node packages?
    Answer: Node Package Manager
*/